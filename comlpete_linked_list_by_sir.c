#include<stdio.h>
#include<stdlib.h>


typedef struct linked_list
{
    int val;
    struct linked_list *next;
}node;

void insert(node *ptr, int val)
{
    while(ptr->next!=NULL){
        ptr = ptr -> next;
    }

    ptr->next = (node*)malloc(sizeof(node));
    ptr = ptr->next;
    ptr->val = val;
    ptr->next = NULL;
}

int find(node *ptr, int key)
{
    ptr = ptr -> next;

    while(ptr!=NULL){
        if(ptr->val == key){
        return 1;
        }
        ptr = ptr -> next;
    }
    return 0;
}

void delete(node *ptr, int val)
{

    while(ptr->next!=NULL && (ptr->next)->val != val)
    {
        ptr = ptr -> next;
    }
    if(ptr->next==NULL)
    {
        printf("Element %d is not present in the list\n",val);
        return;
    }

    node *temp;
    temp = ptr -> next;

    ptr->next = temp->next;

    free(temp);

    return;
}

void print(node *ptr)
{
    if(ptr==NULL){
        return;
    }
    printf("%d ",ptr->val);
    print(ptr->next);
}
int main()
{
    node *start,*temp;
    start = (node*)malloc(sizeof(node));
    temp = start;
    temp -> next = NULL;

    printf("1. Insert\n");
    printf("2. Delete\n");
    printf("3. Print\n");
    printf("4. Find\n");

    while(1)
    {
        int choice, val;
        scanf("%d",&choice);
        switch(choice)
        {
            case 1:
                scanf("%d",&val);
                insert(start,val);
                break;
            case 2:
                scanf("%d",&val);
                delete(start,val);
                break;
            case 3:
                printf("The list is ");
                print(start->next);
                printf("\n");
                break;
            case 4:
                scanf("%d",&val);
                int status = find(start,val);
                if(status)
                    printf("Element Found\n");
                else
                    printf("Element Not Found\n");
        }
    }
}
